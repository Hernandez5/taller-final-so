function edad()
{
	var nombre = prompt("Ingrese su nombre:");
    var ano = prompt("Ingrese su año de nacimiento");
    var mes = prompt("Ingrese su mes de nacimiento en numero");
    var dia = prompt("Ingrese su dia de nacimiento");

	var fecha = new Date(mes+"/"+dia+"/"+ano);
	var hoy = new Date();

    var edad = hoy.getFullYear() - fecha.getFullYear();
    var m = hoy.getMonth() - fecha.getMonth();

    if (m < 0 || (m === 0 && hoy.getDate() < fecha.getDate())) {
        edad--;
    }

    if (edad >= 18) 
    {
    	alert("Bienvenido " + nombre + " puedes acceder a la pagina ya que tienes " + edad + " años y lo requerido son 18");
    }
    else
    {
    	alert("Lo sentimos " + nombre + " pero no puedes acceder a nuestra pagina porque la edad minima es 18 y tu tienes " + edad );
    	document.location.href="https://www.dofus.com/es";
    }
}
edad();