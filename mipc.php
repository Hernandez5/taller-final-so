<!DOCTYPE html>
<html>
<head>
    <title>Santiago Hernandez</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="index.css">
    <link rel="stylesheet" type="text/css" href="mipc.css">
</head>
<body>
    <?php 
        $inicio = "2";
     include 'header.php'; ?>
     <div class="subheader">
    	<nav>
    		<div>
    			<div>
    				<div class="PC">
    					<h1>Asus B360M</h1>
                        <h1>Intel Core I5 9400F</h1>
                        <h1>Memoria RAM XPG 8gb 3000Mhz x2</h1>
                        <h1>HDD Toshiba 3TB 7200rpm</h1>
                        <h1>SSD XPG 256gb M.2 PCIe</h1>
                        <h1>Asus GTX 1050 TI 4gb</h1>
    				=======================================================================================================================
                        <h2>Links de las partes en Amazon</h2>
                            <ul>
                                <li>
                                    <a href="https://www.amazon.com/-/es/Prime-B360M-Intel-Socket-Micro/dp/B07BR8676R"  target="_blank">
                                    Asus B360M
                                    </a>
                                </li>
                                <li>    
                                    <a href="https://www.amazon.com/-/es/Intel-Core-i5-9400F-Procesador-sobremesa/dp/B07MRCGQQ4/ref=sr_1_1?__mk_es_US=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=intel+core+i5+9400f&qid=1586887768&s=electronics&sr=1-1" target="_blank">
                                    Intel Core I5 9400F
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.amazon.com/-/es/Spectrix-CL16-20-20-PC4-25600-Desktop-AX4U3200316G16-DT41/dp/B07FXV8968/ref=sr_1_4?__mk_es_US=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=ram+xpg+8gb+3000mhz&qid=1586887816&sr=8-4" target="_blank">
                                    Memoria RAM XPG 8gb 3200Mhz x2
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.amazon.com/-/es/Toshiba-America-Information-Systems-Desktop/dp/B013JPLOQQ/ref=sr_1_9?__mk_es_US=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=toshiba+hdd+3tb+7200+rpm&qid=1586887891&sr=8-9" target="_blank">
                                    HDD Toshiba 3TB 7200rpm
                                    </a>
                                </li>
                                <li>    
                                    <a href="https://www.amazon.com/-/es/SX8200-Gen3x4-Unidad-estado-ASX8200PNP-256GT-C/dp/B07K1MDMF3?th=1" target="_blank">
                                    SSD XPG 256gb M.2 PCIe
                                    </a>
                                </li>
                                <li>    
                                    <a href="https://www.amazon.com/ASUS-Geforce-Phoenix-Graphics-PH-GTX1050TI-4G/dp/B01M360WG6" target="_blank">
                                    Asus GTX 1050 TI 4gb
                                    </a>
                                </li>
                            </ul>
                    </div>
                    <div class="Imagen PC">
                        <img src="Img/ImagenPC.jpg" width="400"></img>
                    </div>
                </div>
    		</div>
    	</nav>
    </div>
</body>
