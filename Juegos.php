<!DOCTYPE html>
<html>
<head>
    <title>Santiago Hernandez</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="index.css">
    <link rel="stylesheet" type="text/css" href="mipc.css">
</head>
<body>
    <?php 
        $inicio = "2";
     include 'header.php'; ?>
    <div class="subheader">
    	<nav>
    		<div>
                <div>
                    <div class="PC">
                        <a><font size="12">Juegos Recomendados</font></a>
                        <ol>
                        <li><font size="6"> Ancestors: The Human Kind Oddysey </font></li>
                        <img src="Img/ancestors.jpg"></img>
                        <li><font size="6"> FIFA 20 </font></li>
                        <img src="Img/FIFA20.jpg" width="400"></img>
                        <li><font size="6"> Sniper Elite IV (100% Completado) </font></li>
                        <img src="Img/SniperElite4.jpg"></img>
                        <li><font size="6"> Rust </font></li>
                        <img src="Img/Rust.jpg" width="400"></img>
                        <li><font size="6"> Ark Evolved </font></li>
                        <img src="Img/arkevolved.jpg" width="400"></img>  
                        <li><font size="6"> Arma III </font></li>
                        <img src="Img/arma3.jpg" width="400"></img>
                        <li><font size="6"> Dofus 2004 </font></li>
                        <img src="Img/dofusretro.jpg" width="400"></img>
                        <li><font size="6"> Call Of Duty:MW Remastered (100% Completado) </font></li>
                        <img src="Img/CODMWR.jpg" width="400"></img>
                        <li><font size="6"> Call Of Duty:WW II </font></li>
                        <img src="Img/CODWW2.jpg" width="400"></img>
                        <li><font size="6"> Call Of Duty:WarZone </font></li>
                        <img src="Img/CODWZ.jpg" width="400"></img>
                        <li><font size="6"> Path Of Exile </font></li>
                        <img src="Img/Poe.jpg" width="400"></img>
                        <li><font size="6"> Fortnite </font></li>
                        <img src="Img/Fortnite.jpg" width="400"></img>
                        <li><font size="6"> Subnautica (100% Completado) </font></li>
                        <img src="Img/Subnautica.jpg" width="400"></img>
                        <li><font size="6"> Subnautica:BelowZero (100% Completado) </font></li>
                        <img src="Img/BelowZ.jpg" width="400"></img>
                        <li><font size="6"> GTA V (100% Completado) </font></li>
                        <img src="Img/GTAV" width="400"></img>
                        <li><font size="6"> Tomb Raider (100% Completado) </font></li>
                        <img src="Img/TombR.jpg" width="400"></img>
                        <li><font size="6"> Rise of The Tomb Raider </font></li>
                        <img src="Img/RiseT.jpg" width="400"></img>
                        <li><font size="6"> Shadow of The Tomb Raider (100% Completado) </font></li>
                        <img src="Img/Shadowt.jpg" width="400"></img>
                        <li><font size="6"> Microsoft Flight Simulator X </font></li>
                        <img src="Img/FlightX.jpg" width="400"></img>
                        <li><font size="6"> Red Dead Redemption II </font></li>
                        <img src="Img/RDR2.jpg" width="400"></img>
                    	</ol>
                    </div>
                </div>
            </div>    		
    	</nav>
    </div>
</body>
</html>